import yaml
from bs4 import BeautifulSoup
import json

with open("faq.html", "r", encoding="utf-8") as f:
    contents = f.read()
    soup = BeautifulSoup(contents, 'html.parser')

questions = soup.findAll("h3", {"class":"question__txt"})
answers = soup.findAll("div", {"class": "question__answer-txt"})

questions = [q.text for q in questions]
answers = [a.text.replace("\n", "") for a in answers]

with open("faq_intent_names.json") as f:
    names = json.load(f)

intents = dict(zip(
    ["## intent: chitchat/"+n for n in names],
    [[q] for q in questions]))

responses = dict(zip(
    ["* chitchat/" + n for n in names],
    [[a] for a in answers]
))
responses = {"## " + k.replace("_", " ").replace("* chitchat/", ""): {k: v} for k, v in responses.items()}
responses
with open("intents.yaml", "w") as f:
    yaml.dump(intents, f)

with open("responses.yaml", "w") as f:
    yaml.dump(responses, f)
