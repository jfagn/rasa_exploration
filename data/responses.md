## US coverage
* chitchat/US_coverage
  - This depends on a lot of things, like where you're going and how long you'll begone. It's a good idea to have a chat with one of our licenced insurance agents.They can assess the situation and let you know whether your car insurance coverageis good as it is or whether you need to make some changes.
## aging car premium
* chitchat/aging_car_premium
  - There are several factors we take into consideration when we calculate your carinsurance premium. The two main factors are the cost of parts (which don't alwaysdecrease, like the value of the vehicle), and the cost of the repairs (we needto pay the mechanics who repair your vehicle). We also take the type of vehicleinto consideration (if it had a high rate of claims for vehicle damage over thepast year).
## at fault decision
* chitchat/at_fault_decision
  - "In Ontario, we follow a set of rules known as 'Fault Determination Rules.' These rules look at all kinds of possible accident scenarios, without factoring in visibility, road conditions, weather, or pedestrian actions. If these rules aren't applicable, we look at the ordinary rules of law."
## at fault definition
* chitchat/at_fault_definition
  - "At an-fault accident is when you're considered responsible for an accident you're involved in."
## attendant definition
* chitchat/attendant_definition
  - An attendant is a caregiver who helps an injured person get through the tasksof their daily routine.
## atv outside province
* chitchat/atv_outside_province
  - You can leave for your trip feeling secure thanks to our insurance coverage thatallows you to use your vehicle outside of the province for a maximum of 180 consecutivedays at no extra cost! We recommend that you let your insurer know if you decideto use your vehicle outside of the province.You can leave for your trip feelingsecure thanks to our insurance coverage that allows you to use your vehicle outsideof the province for a maximum of 180 consecutive days at no extra cost! We recommendthat you let your insurer know if you decide to use your vehicle outside of theprovince.You can leave for your trip feeling secure thanks to our insurance coveragethat allows you to use your vehicle outside of the province for a maximum of 180consecutive days at no extra cost! We recommend that you let your insurer knowif you decide to use your vehicle outside of the province.
## automerit additional drivers
* chitchat/automerit_additional_drivers
  - Yes! Any driver listed as a principal driver on your policy can join the automeritprogram.
## automerit battery usage
* chitchat/automerit_battery_usage
  - Like any app that uses your location, the automerit program will consume an averageof 3% to 6% of your smartphone's battery life per hour. This is a small amountwhen you're not driving, and slightly more while you're taking a trip. Remember,if your battery is lower than 20% or your smartphone is in power-saving mode,your trips will not be recorded!
## automerit behaviour display
* chitchat/automerit_behaviour_display
  - The automerit program helps you drive safer by translating your driving data intoeasy to understand driving behaviours and events, such as:hard braking;rapid acceleration;riskyhours;speed; anddistracted driving (using your phone to talk or text, even ifit's hands-free).This is simply an overview as several factors are used to calculateyour automerit discount.
## automerit business use
* chitchat/automerit_business_use
  - "Not a problem. If you're driving for business, your trips will still count towards your Safety Scoreand automerit discount.Not a problem. If you are driving your personal car for business, e.g., if you're a realtor or salesperson, your trips will still count towards your Safety Score and automerit discount."
## automerit calculation
* chitchat/automerit_calculation
  - Your automerit discount is based on the following driving events:duration of trip;hardbraking;rapid acceleration;cornering and fast turns;kilometres driven; anddistracteddriving (using your phone to talk or text, even if it's hands-free).braking (e.g.,harsh braking);acceleration (e.g., sudden acceleration); turning (e.g., cornering);distancedriven;time of day;type of road driven on; /li>                    speed; anddistracteddriving.These driving events are then analyzed based on factors such as:severityof the event;speed at which the event was registered;time of day andday of theweek; androad types (e.g., highway, urban or rural); andleft or right turns.Weunderstand that even experienced drivers need to brake hard or accelerate quicklysometimes. It's your safety that matters most, which is why your automerit discountis calculated over the average of several trips. Don't worry! One individual eventwill not have a significant impact on your overall score.
## automerit call tracking
* chitchat/automerit_call_tracking
  - No, belairdirect only receives information to detect if you are using your phonewhile driving. Your phone calls will never be tracked or recorded.
## automerit data plan
* chitchat/automerit_data_plan
  - Yes, you must have a data plan with internet access in order to collect and transferyour driving data to us.
## automerit data protection
* chitchat/automerit_data_protection
  - We use strict security safeguards when storing or destroying your personal informationin order to prevent unauthorized access, collection, use, disclosure, copying,modification, disposal or similar risks. In the event that we transfer your personalinformation to a third party (e.g. our service provider, TrueMotion, Inc.), wecontractually require such third party to protect and handle your personal informationin a manner consistent with our own privacy safeguarding measures and accordingto all applicable laws.
## automerit data sharing
* chitchat/automerit_data_sharing
  - belairdirect and Truemotion, Inc. will only use and disclose your informationin accordance with the automerit program's Terms of Use, unless otherwise requiredby law.
## automerit data usage
* chitchat/automerit_data_usage
  - "Generally, between 30 and 60 MB per month will be used. This is a very small fraction of the average data plan (1000 - 2000 MB or more per month)."
## automerit definition
* chitchat/automerit_definition
  - 'Available through the belairdirect app, automerit is belairdirect''s usage-basedinsurance program that rewards safe driving behaviour. Through your smartphone''sGPS, accelerometer, and gyroscope, the automerit program will assess how safelyyou drive in order to calculate your potential discount. Approximately every 6months, you could save up to 25% on your car insurance based on the driving choicesyou make. It''s simple: the safer you drive, the more you could save!Availablethrough the belairdirect app, automerit is belairdirect''s usage-based insuranceprogram that rewards safe driving behaviour. Through your smartphone''s GPS, accelerometer,and gyroscope, the automerit program will assess how safely you drive in orderto calculate your potential discount. Get a 5% enrolment discount just for signingup and if you drive safely, you could save up to 25% off your car insurance. It''ssimple: the safer you drive, the more you could save!'
## automerit delete app
* chitchat/automerit_delete_app
  - Don't worry! If you delete the belairdirect app by mistake, you just need to downloadit again and follow the prompts to activate the automerit program. All your informationwill be stored, and you can continue driving. Remember - if we can't record yourtrips within a 30-day period this could result in the cancellation of your automeritprogram enrolment and you will lose any discount related to the program.
## automerit discount application
* chitchat/automerit_discount_application
  - Your automerit discount is just one of many savings that could be applied to yourcar insurance policy (e.g. car + home discount, discount for two or more vehicles,etc.). Discounts are applied to your premium in different sequences and applyto specific coverage, which is why you might notice that your dollar discountis different from your percentage discount. Don't worry! You will always receivethe correct amount of savings.
## automerit discount mismatch
* chitchat/automerit_discount_mismatch
  - The discount that is applied on your policy documents is the one calculated approximatelyevery 6 months, at mid-term or at renewal, and could be different from the projecteddiscount that appears in the app today, which is updated after every trip.
## automerit discount storage
* chitchat/automerit_discount_storage
  - Your automerit discount can't be applied to a car in storage. However, if youdrive are the principal driver of more than one car, your discount will be appliedto the car you're currently driving. Your automerit discount or high-risk surchargecan't be applied to a car in storage. However, if you drive more than one car,your discount or surcharge will be applied to the car you're currently driving.
## automerit discount update time
* chitchat/automerit_discount_update_time
  - Your projected automerit discount will appear in the app once you have driven1,000 km. Once this threshold is reached, your projected discount will be updatedwithin a few minutes after each trip.
## automerit download
* chitchat/automerit_download
  - "Depending on your smartphone settings, the belairdirect app will update automatically with the automerit program or you will be prompted to confirm the update. If you don't have the belairdirect app, you can download it for free from the App Store or Google Play."
## automerit driver detection
* chitchat/automerit_driver_detection
  - When you first access the automerit program, you will be asked if you're mostlythe driver or a passenger when travelling by car. We also use state-of-the-arttechnology to analyze your driving behaviour and smartphone's movements in orderto identify if you're the driver, passenger, or travelling by some other modeof transit (e.g. public transportation).
## automerit driving data police
* chitchat/automerit_driving_data_police
  - Not without proper legal authorization or your consent. We're committed to protectingyour privacy and will never disclose your personal information to law enforcementagencies, unless otherwise permitted or required by law.
## automerit driving stats
* chitchat/automerit_driving_stats
  - Your Driving Stats provide you with a detailed description of each risk displayedin the app and propose tips for improvement. This allows you to monitor your progressand see how you compare to other drivers participating in the automerit programin your province.
## automerit enrolment
* chitchat/automerit_enrolment
  - "Joining the automerit program is quick and easy! Simply contact us, or for more information visit www.belairdirect.com/automerit. A download link will be texted to you and each principal driver on your policy who has joined the program.* Once you activate your account, accept the Terms of Use, and give the app access to certain features on your smartphone - you're ready to start driving!Joining the automerit program is quick and easy! Simply contact us, or for more information visit www.belairdirect.com/automerit. All drivers registered for automerit under the same policy will receive a text message (SMS) with instructions to activate the program.* Once you activate your account, accept the Terms of Use, and give the app access to certain features on your smartphone - you're ready to start driving!\*If there's more than one principal driver on the same policy enrolled in the automerit program, the download link will be texted to the phone number provided for each principal driver.Joining the automerit program is quick and easy. It's automatically included in your car insurance coverage and a download link will be texted to you and each principal driver on your policy. Once you activate your account, accept the Terms of use, and give the app access to certain features on your smartphone-you're ready to start driving! "
## automerit enrolment discount
* chitchat/automerit_enrolment_discount
  - "The enrolment discount is applied to the cost of your car insurance when you first join the automerit program. This one-time discount is applicable over your first collection period of approximately 6 months. At the end of this collection period, you could save with a personalized driving discount based on your driving behaviour.The enrolment discount is applied to the cost of your car insurance when you first join the automerit program. This one-time discount is applicable over your first collection period of approximately 6_months. At the end of this collection period, you could save with a personalized driving discount based on your driving behaviour."
## automerit error management
* chitchat/automerit_error_management
  - "We're here to help! Contact us at  1_877_727.8477 1_855_250.6517 1_844_364.0223  1_877_220.6065  1_877_727.8477 1_855_250.6517 1_844_364.0223  1_877_220.6065  if there's an error in your driving data, or if you're experiencing technical difficulties."
## automerit info other drivers
* chitchat/automerit_info_other_drivers
  - No. This information is only visible to you and is not shared with any other driverson your policy.
## automerit mileage need
* chitchat/automerit_mileage_need
  - In order for us to capture an accurate picture of your driving behaviour, youmust drive a minimum of 1,000 km during a collection period. If you don't, anyautomerit discount will cease to apply until a new collection period is completed.
## automerit missing trip
* chitchat/automerit_missing_trip
  - It may occasionally happen that a trip is not recorded due to:no internet connection;batteryis lower than 20%;smartphone is in power-saving mode;permission is disabled;appis not running in the background;app is experiencing technical difficulties.
## automerit multiple cars
* chitchat/automerit_multiple_cars
  - Yes, your automerit discount will apply to any car you drive as long as you'rethe principal driver on the policy, and your car meets the eligibility requirementsof the program.Yes, your automerit discount or high-risk driving surcharge willapply to any car you drive as long as you're the principal driver on the policy,and your car meets the eligibility requirements of the program.Yes, your automeritdiscount will apply to any car you drive as long as you're the principal driveron the policy, and your car meets the eligibility requirements of the program.
## automerit need call
* chitchat/automerit_need_call
  - We need access to "make and manage phone calls" on your Android device for itsaccelerometer and gyroscope sensors. As the name implies, the accelerometer measuresacceleration and helps us determine how fast your phone is moving. Whereas thegyroscope detects which direction it's pointing in.
## automerit need location
* chitchat/automerit_need_location
  - The automerit program won't work unless you turn on location services in yoursmartphone settings. Your location allows us to detect driving behaviour, suchas changes in speed and when you start a trip. It also helps us to identify whetheryou're the driver or a passenger.
## automerit need sensor
* chitchat/automerit_need_sensor
  - We need access to your iPhone's motion and fitness data for its accelerometerand gyroscope sensors. As the name implies, the accelerometer measures accelerationand helps us determine how fast your phone is moving. Whereas the gyroscope detectswhich direction it's pointing in.
## automerit new number
* chitchat/automerit_new_number
  - Just give us a call! We'll make the necessary changes to your account and textyou a new automerit download link. All your information will be stored, and youcan continue driving.
## automerit new phone
* chitchat/automerit_new_phone
  - You will need to download the belairdirect app again. There's no need to re-enrolin the automerit program as all your information will be stored.
## automerit no activation
* chitchat/automerit_no_activation
  - 'It''s important to activate your enrolment in the automerit program within 15days of the program''s effective date. If you don''t, this could result in thecancellation of your enrolment and you will lose any discounts related to theprogram. '
## automerit no driving
* chitchat/automerit_no_driving
  - Please contact us in advance if you're planning not to drive for more than 30days. If you don't, this could result in the cancellation of your automerit enrolmentand you will lose any discount related to the program.
## automerit not driver history
* chitchat/automerit_not_driver_history
  - Don't worry! In the event that a trip has been misclassified, visit the All Tripssection of the app and set the toggle to "I was a passenger on this trip" within30 days of completing the trip. You can also contact us if you need to correctany other tagged information.
## automerit not in app
* chitchat/automerit_not_in_app
  - "If you already have the belairdirect app and cannot access the automerit program in the app, please try logging out and logging back in. If you still cannot access the program, try deleting the app and downloading it again or contact us at  1_877_727.8477 1_855_250.6517 1_844_364.0223  1_877_220.6065  1_877_727.8477 1_855_250.6517 1_844_364.0223  1_877_220.6065   and a member of our customer service team will guide you through activating the automerit section of the app"
## automerit passenger using phone
* chitchat/automerit_passenger_using_phone
  - Just visit the All Trips section of the app and set the toggle to "A passengerwas using my phone" within 30 days of completing the trip. The distracted drivingevent will then be removed from your Trip Summary. You can also contact us ifyou need to correct any other tagged information.
## automerit payment adjustment
* chitchat/automerit_payment_adjustment
  - 'Whenever the cost of your car insurance changes, you will receive updated policydocuments including a billing statement indicating any adjustments in your payments:Ifthe premium has already been paid in full: a refund will be issued if the premiumdecreased, or an additional amount will be due if the premium has increased;Ifthe premium is paid monthly, or by another payment method: the payment schedulewill be adjusted accordingly to reflect increased or decreased payments, as thecase may be.When your policy is renewed, the cost of your car insurance may increaseor decrease based on factors other than a change in your automerit discount.'
## automerit premium update
* chitchat/automerit_premium_update
  - When you first join the automerit program, you will receive a one-time 10%5% enrolmentdiscount. This enrolment discount will then be replaced, if applicable, by a personalizeddriving discount based on your driving behaviour (180 days after your first taggedtrip).For a collection period ending during your policy term, you can expect that:Ifyour discount increases, your automerit discount and premium will be updated.Ifyour discount decreases, your automerit discount and premium will be updated.However, if your discount only decreases by less than 7% 5%, we will not applyyour updated automerit discount and will continue to apply the previous discountand premium.Your automerit discount will again be adjusted when your policy isup for renewal. Once your policy is renewed, the cost of your car insurance mayincrease or decrease based on several factors other than a change in your automeritdiscount. For upcoming policy terms, your discount will be updated approximately6 months into your policy term, and again at renewal.Once your first collectionperiod has been completed (180 days after your first trip), if applicable, yourautomerit discount or a high-risk driving surcharge will be applied. For a collectionperiod ending during your policy term, you can expect that:If your discount increases,your automerit discount and premium will be updated.If your discount decreases,your automerit discount and premium will be updated. However, if your discountonly decreases by less than 5%, we will not apply your updated automerit discountand will continue to apply the previous discount and premium.Your automerit discountor high-risk driving surcharge will be adjusted when your policy is up for renewal.Once your policy is renewed, the cost of your car insurance may increase or decreasebased on several factors other than a change in your automerit discount. For upcomingpolicy terms, your discount or high-risk driving surcharge will be updated approximately6 months into your policy term, and again at renewal.
## automerit safety score definition
* chitchat/automerit_safety_score_definition
  - 'Your Safety Score provides you with an at-a-glance summary of how safe you''redriving. It takes into account your driving data from the last 30 days, and willdisplay a score for the following five risks which are highly representative ofyour driving and could affect your automerit discount:Risky hours: driving atnight between 2 a.m. and 5 a.m. when the risk for car accidents is higher.Rapidacceleration: quick changes in speed starting at or exceeding 11.0 km/hr per second.Hardbraking: sudden changes in braking speed starting at or exceeding -11.0 km/hrper second.Speeding: driving at an excessive speed.Distracted driving: using yourphone to talk or text, even if it''s hands-free.We understand that even experienceddrivers need to brake hard or accelerate quickly sometimes. It''s your safetythat matters most, which is why your Safety Score is calculated over the averageof several trips. Don''t worry! One individual event will not have a significantimpact on your overall score.'
## automerit safety score reading
* chitchat/automerit_safety_score_reading
  - 'Your Safety Score lets you take a closer look at how you''re driving throughcolour coding to indicate your level of risk:Dark green: very low riskLight green:low riskYellow: moderate riskOrange: high riskRed: severe risk'
## automerit see discount
* chitchat/automerit_see_discount
  - Your projected automerit discount will be displayed on the app's dashboard. Everymonth, the policyholder will receive an email outlining any upcoming discountfor all principal drivers on the same policy.Your projected discount or high-riskdriving surcharge will be displayed on the app's dashboard. Every month, the policyholderwill receive an email outlining any upcoming discount or surcharge for all principaldrivers on the same policy.
## automerit smartphone
* chitchat/automerit_smartphone
  - You will need an iPhone running iOS 10 or higher, or an Android device runningversion 5.1 or higher. Depending on the type of smartphone you have, the automeritprogram's performance can vary as certain sensors behave differently in differentdevices.
## automerit start mismatch
* chitchat/automerit_start_mismatch
  - In order to determine your trip's starting point, you need to start driving. Asa result, there's a delay of 300 metres before we can detect movement and pinpointyour location.
## automerit surcharge
* chitchat/automerit_surcharge
  - Your personalized driving discount may change or even be removed during the courseof your policy term. Mid-term (at approximately 6 months) your premium may increaseor decrease due to the completion of a collection period and the updating of yourautomerit discount. This fluctuation could result in a balance owing to us. Whenyour policy is renewed, your automerit discount will be adjusted and your premiumcould again increase or decrease based on several factors other than a changein your automerit discount.
## automerit trip access
* chitchat/automerit_trip_access
  - Information about your driving behaviour and trips is only shared with TrueMotion,Inc., a mobile telematics company. We work with this service provider for thetechnology, systems, and services necessary to run the automerit program.
## automerit uber
* chitchat/automerit_uber
  - "The automerit program is designed for business and personal use only. If you're driving for commercial reasons (Uber, Lyft, etc.), you will need to visit the All Trips section of the app and set the toggle to "I was a passenger on this trip" within 30 days of completing the trip. You can also contact us if you need to correct any other tagged information.The automerit program is designed for business and personal use only. If you are driving for commercial purposes, for example if you're an Uber or Lyft driver, ambulance driver, bus driver or heavy truck driver, you must visit the 'All Trips'. You can also contact us if you need to correct any other tagged information."
## automerit withdrawral
* chitchat/automerit_withdrawral
  - Yes, you can end your participation in the automerit program at any time by contactingus. However, you will lose any discount related to the program.Yes, you can endyour participation in the automerit program at any time by contacting us. However,you will lose any discount related to the program.
## basic property insurance
* chitchat/basic_property_insurance
  - We have several home insurance forms, and the form best suited to your needs willbe offered by the agent at the time of submission. This form may be changed duringthe term or at your renewal, depending on your changing needs and situation.
## belongings in car
* chitchat/belongings_in_car
  - Your belongings are covered under your home insurance policy. So if you make aclaim only for your belongings, it will be listed on your home insurance. If youmake a claim for both your car and your belongings and you're insured with belairdirectfor both lines of insurance, you only pay one deductible (the higher one).
## belongings replacement value
* chitchat/belongings_replacement_value
  - "It depends whether or not you have Replacement Cost coverage. Here's the breakdown:With Replacement Cost coverage:You'll get the lesser amount that comes out of the two scenarios below:1. If your laptop is found but is damaged, the repairs are covered with no deduction for depreciation. OR2. You'll get compensated the value of a new laptop, identical or equivalent to the original, and the same type and quality.Without Replacement Cost coverage:You'll get the actual cash value of the laptop on the day it was stolen. This amount is based on how much the laptop is worth minus depreciation, which is calculated by looking at the condition of it, its resale value, and its normal life expectancy."
## burgler action
* chitchat/burgler_action
  - "When theft occurs in your home, it can be a traumatic experience. We understand how it can impact your sense of security, and we will help you recoup your losses as fast as possible. Here's what you can do if it happens to you.Call the police and report the theft right away.Try to see if any damage has been done to the inside or outside of your home.Call us. Our claims representatives can start your claim and help you decide what to do next.If you are renting your home, you should call the owner to let him/her know about the situation, especially if there is damage to the building.Your home may need to be secured (if a window or door is broken, for example). If it's urgent, call a contractor to make the necessary repairs and be sure to get receipts. We can put you in touch with someone from our Rely Network to do the job for you. Try to make a list of the possessions that are missing. Include dates purchased, place of purchase, make and model, serial numbers and any proofs of possession (receipts, pictures, instruction manuals, boxes or account statements)."
## car damage action
* chitchat/car_damage_action
  - "Here are some steps to follow, whether your car has suffered a minor fender bender or major structural damage. Make sure you're safe and that your car is not a danger for others, then gather all the necessary information you need to begin the claim process.Take a moment to check that you and any passengers in your car haven't been injured. Your health is the most important thing to consider. If your car is still drivable, move your vehicle to a safe location out of traffic right away. Even if you think you may be responsible, do not make a decision in the moment that may cost you money later.You and the other driver should provide each other with your name, address, telephone number, driver's licence number, insurance company name and policy number, and year/make/model of vehicle.Look for witnesses. If someone saw your accident, you may want to want to ask if you can contact them to support your version of events.Report it to the police. In cases where the damage is slight and no one was hurt, many people think it's not important to report the incident. But if there is any damage at all, reporting it to the police is a good rule of thumb.Get to the nearest repair shop. In most cases, if your vehicle cannot be driven, a tow truck is legally obligated to bring it to the nearest Collision Reporting Centre. You can contact us from there to talk about your next steps, including taking your car to a repair shop from our Rely Network.Contact us as soon as possible. We can confirm your insurance coverage, tell you if you're entitled to any services or to a rental car, and recommend a preferred repair shop. You can reach a belairdirect claims representative at 1 877 270.9124, 24 hours a day, 7 days a week.Only fix what needs to be fixed. Please contact us before making any other repairs."
## car insurance discount
* chitchat/car_insurance_discount
  - Every insurance company is different, but most will offer you ways to save onyour insurance premium. When you're insured with belairdirect, you've got manyways to save:Insuring your car and home togetherInsuring more than one car onthe same policyIncreasing your deductibleBeing a good driverTaking steps to decreaseyour risk
## car insurance obligation
* chitchat/car_insurance_obligation
  - Yes. If you own a car, you must have a minimum amount of insurance. It's the law.
## car market value
* chitchat/car_market_value
  - We'll bring in an appraiser who will look up the going rate for cars like yoursin your area. They'll also look at the condition of your car before the accident,paying attention to things like mileage, options, and any damage that was therebefore the accident.
## car repairs
* chitchat/car_repairs
  - "Ultimately, you can go anywhere you'd like, but we're more than happy to recommend one of the certified repair shops in our Rely Network. We trust these shops to get the job done right, and the repairs are guaranteed for as long as you own your car. Also, you can rest easy knowing your belairdirect coverage gives you priority treatment in these shops."
## car storage
* chitchat/car_storage
  - Yes. You're still the owner of this vehicle and are still responsible for it,even if it's in storage. Any damage that happens to your vehicle during that timewouldn't be covered if you don't have insurance.
## car type impact
* chitchat/car_type_impact
  - Yes, it absolutely does. Insurance companies have a ton of statistics that show,in general, which vehicles have the most and least claims attached to them bymake, model and year. More claims generally equal higher risk to insure and viceversa. Some vehicles are harder to repair, or may not have parts readily available.These also affect premiums.
## catastrophic injury definition
* chitchat/catastrophic_injury_definition
  - "A catastrophic injury is an extremely serious injury that can end up as a permanent disability or long-lasting medical condition that fundamentally changes someone's life."
## child learning
* chitchat/child_learning
  - Yes. If you ever need to file a claim, damages won't be covered if your son/daughterisn't mentioned on the policy. It's important to insure any new driver, even ifthey don't drive often and only have a learner's permit.
## city to suburb
* chitchat/city_to_suburb
  - Potentially. When you move, we will need to take a look at the changes in yoursituation. Things like the distance you drive to work every day and where youlive will affect your premium.
## civil liability definition
* chitchat/civil_liability_definition
  - A civil liability is your financial responsibilities when your actions cause harmto someone and/or damage to their property..
## claim documents
* chitchat/claim_documents
  - "Here's what we'll ask you for when you start your claim:Your policy number (or a phone number if you don't have your policy number handy)As many details as possible regarding the incidentA police or fire department report (if one was made)For equipment or appliances, documentation includes things like invoices, receipts, photos, make/model/serial numbers (or even user guides and warranties). For jewellery or art, appraisals are the best."
## claim impact premium
* chitchat/claim_impact_premium
  - Generally, most claims will affect your insurance premium, especially an at-faultclaim. But if you have The Accident Forgiveness from belairdirect, your insurancepremium won't go up at renewal as a direct result of your first two at-fault accidents.Interested in adding The Accident Forgiveness to your coverage? It's easy.
## commercial car personal use
* chitchat/commercial_car_personal_use
  - It always depends on your insurance contract with your company. At belairdirect,normally, personal use is covered by the same contract as your commercial or professionalinsurance. In case of doubt, do not hesitate to contact us.
## daughter car insurance
* chitchat/daughter_car_insurance
  - When your son or daughter starts to drive and gets insured as a primary driver,he or she will start getting insurance experience that can affect his/her premiumwhen he/she has his/her own car. Also, he or she may be eligible for our occasionaldriver discount if he/she has his/her learner's permit and is under the age of25.
## deductible explanation
* chitchat/deductible_explanation
  - A deductible is the sum deducted from part of a claim that you need to pay yourself.In other words, it's the amount that's deducted from the total you could get fromus. Deductibles are different depending on the type of claim (whether it's forCollision and upset or All perils other than collision or upset). For Collisionand upset coverage, you can usually choose between $250 and $500, and for Allperils other than collision or upset coverage, you can choose $50, $100, $250or $500. Please keep in mind that with every different situation, conditions mayapply.
## dependant definition
* chitchat/dependant_definition
  - A dependant is someone, often a family member, who depends on another person forfinancial support.
## depreciation definition
* chitchat/depreciation_definition
  - A depreciation is when something goes down in value because of wear and tear,age, or by becoming obsolete.
## difference partial total loss
* chitchat/difference_partial_total_loss
  - A partial loss is when a car is damaged but not completely destroyed. On the otherhand, a total loss is when something is damaged beyond use or repair.
## digital insurance access
* chitchat/digital_insurance_access
  - You can access your digital proof of insurance in the Car or Documents sectionof the app. For secure access, add the widget to your iPhone or Android device.If you have an iPhone, you can also use the Wallet app. Visit the help page foryour  iPhone or Android device, and follow the instructions.
## digital insurance commercial
* chitchat/digital_insurance_commercial
  - As long as the vehicle is covered by a personal auto insurance policy, you canaccess your digital proof of insurance through the app or your Client Centre account.
## digital insurance info
* chitchat/digital_insurance_info
  - On your digital proof of insurance you will find the name of your insurance companywith your policy number, coverage dates, name, address, as well as the year, make,model and the serial number of your vehicle.
## digital insurance offline
* chitchat/digital_insurance_offline
  - When you renew or make changes to your policy, the changes will appear the nextbusiness day in the app and widget.If you have the Wallet app, visit the Car orDocuments section of the app and select "Add to Apple Wallet" to get the latestversion.When you add the widget to your iPhone or Android device or use the Walletapp, you can access your digital proof of insurance even when you're offline.
## digital insurance outside province
* chitchat/digital_insurance_outside_province
  - The digital version of your proof of insurance is valid in Quebec, Ontario, Alberta,Nova Scotia, Newfoundland and Labrador. Make sure you always carry the paper copywhen travelling outside the province.
## digital insurance personal info protection
* chitchat/digital_insurance_personal_info_protection
  - We're committed to protecting your privacy, which is why our digital proof ofinsurance is available on your phone's lock screen. This feature allows you tolock your screen and restrict access to your phone's content when sharing yourdigital proof of insurance. For added protection, you can also turn off notifications,but remember to be careful not to unlock your phone through fingerprint or facialrecognition accidentally. By using this digital proof of insurance, you acceptall risk and responsibility associated with its use.
## digital insurance phone damage
* chitchat/digital_insurance_phone_damage
  - If you choose to use digital proof of insurance, you're responsible for any risksthat may arise, such as damage to your phone by a third party, including a policeofficer.
## digital insurance print
* chitchat/digital_insurance_print
  - If you need to print your proof of insurance, download the PDF version availablein the app or your Client Centre account. Print it in colour on white paper tomake sure all the information is clear and legible. We recommend that you alwayskeep the paper copy of your proof of insurance in your vehicle when travellingoutside the province or to share with other drivers and law enforcement.
## digital insurance proof
* chitchat/digital_insurance_proof
  - Digital proof of insurance is valid in Quebec, Ontario, Alberta, Nova Scotia,Newfoundland and Labrador. Make sure you always carry the paper copy when travellingoutside the province. The digital version of your proof of insurance is availablein the app. However, this is not a valid proof of insurance in your province.Make sure you always carry the paper copy.
## digital insurance refusal
* chitchat/digital_insurance_refusal
  - "The digital version of your proof of insurance is valid in Quebec, Ontario, Alberta, Nova Scotia, Newfoundland and Labrador. If necessary, a police officer can confirm your coverage by calling 1_888 270.9732 1_888 280.8549 1_866 473.9659 1_844 757.2925 1_866 473.9676.We recommend that you always keep the paper copy of your proof of insurance in your vehicle when travelling outside the province or to share with other drivers and law enforcement.Tips on how to print your proof of insurance.The digital version of your proof of insurance is valid in Quebec, Ontario, Alberta, Nova Scotia, Newfoundland and Labrador. If necessary, a police officer can confirm your coverage by calling 1_866.473.9659 or you can present the paper copy.In Alberta, you also have the option of emailing your digital proof of insurance to law enforcement if required.We recommend that you always keep the paper copy of your proof of insurance in your vehicle when travelling outside the province or to share with other drivers and law enforcement."
## digital insurance share
* chitchat/digital_insurance_share
  - If you have an iPhone, use the Wallet app and select the 3-dot menu to accessthe share feature. You can also use AirDrop, iMessage, or email to share yourdigital proof of insurance. It's not possible to share your digital proof of insurancewith an Android device. Only policyholders* have access through the app. In themeantime, make sure you always keep the paper copy in your vehicle.\*The policyholderis the person whose name the insurance policy is registered.If you have an iPhone,use the Wallet app and select the 3-dot menu to access the share feature. Youcan also use AirDrop, iMessage, or email to share your digital proof of insurance.It'snot possible to share your digital proof of insurance with an Android device.Only policyholders* have access through the app, but you can email a PDF version.\*Thepolicyholder is the person whose name the insurance policy is registered.
## domestic international encryption
* chitchat/domestic_international_encryption
  - 'The difference between these two types of encryption is one of capability. Domestic-gradeencryption is exponentially more powerful than international-grade encryption.Think of it this way: 40-bit encryption, also called international-grade encryption,means there are 240 possible keys that could fit into the lock holding your accountinformation. Meaning there are many billions (a 1 followed by 9 zeroes) of possiblekeys. 128-bit encryption, also called domestic-grade encryption, means there are288 (a three followed by 26 zeroes) times as many key combinations than thereare for 40-bit encryption. A computer would require exponentially more processingpower than for 40-bit encryption to find the correct key.'
## encryption definition
* chitchat/encryption_definition
  - "Everything that travels through cyberspace during your online session, from your password to your instructions and commands, becomes a string of unrecognisable numbers before it is transmitted via the Internet. Both belairdirect's computers and the browser you use to surf the Web understand the mathematical formulas, called algorithms, that turn your online session into numeric code, and back again to meaningful information.These algorithms serve as locks on the vaults of your account information. And while belairdirect and your computer can easily translate this code back to a meaningful language, this process would be a daunting, almost impossible task for unauthorised intruders. That's because there are billions of possible keys potentially solving each formula -- but only one that will work. Each time you begin an online session, your computer and belairdirect's systems agree on a random number to serve as the key for the rest of the conversation. What the random number could be depends largely on the strength of encryption your browser utilises."
## encryption need
* chitchat/encryption_need
  - "There are currently two levels of encryption available in today's popular web browsers: 40-bit encryption and 128-bit encryption. We recommend that you use a 128- bit browser, because it provides a much higher level of security. We've provided download locations for Microsoft's Internet Explorer, Firefox and Safari. We can also help you determine whether your browser is secure enough for online browsing, and we can help you download a browser with the encryption you need."
## endorsement definition
* chitchat/endorsement_definition
  - An endorsement is an addition, exclusion or modification made to your standardinsurance policy.
## equipment coverage
* chitchat/equipment_coverage
  - 'Quebec: Yes! You''re automatically covered for optional equipment and accessories.Off-roadVehicles                 You are automatically covered for up to $3,000 worthof equipment and accessories that you''ve added to your ATV or snowmobile. Motorcycle,Scooter and Moped*                 You are automatically covered for up to $4,000worth of equipment and accessories such as foot rests, leather saddlebags, stereosystems and chrome parts.That''s what you get with our standard insurance coverage.You can increase your coverage for a small additional premium.* Does not applyto mopeds 50cc or less.'
## fire damage action
* chitchat/fire_damage_action
  - "Even on a small scale, fire can be damaging to possessions and -more seriously- to health. If a fire occurs in your home, this checklist can help you deal with the aftermath. And we'll be ready to help you repair the damage and return your home to its previous state.Call the fire department. Even if the fire is small, get the experts to your home right away.The fire department will let you know when it's safe to go back into your home or into a particular room.Call us. Our claims representatives can start your claim and help you decide what to do next.If you can't live in your home due to the fire, one of our claims representatives can help you relocate until your home is back to normal.Ask for a fire and police report. This is important for us to complete your claim quickly and to your satisfaction."
## fire insurance coverage
* chitchat/fire_insurance_coverage
  - Fire damage is covered by the standard contract. Some other damage, such as windand hail damage, may also be included. Other damages may be added to the contractat a later date. It is important to always refer to your contract, and in caseof doubt or questions to speak with an insurance agent.
## fraud prevention
* chitchat/fraud_prevention
  - Answer Digital proof of insurance is equipped with several safety features toprevent fraud and protect your privacy. The app will never allow access to yourlocation or personal information without your explicit consent.
## frequent flyers
* chitchat/frequent_flyers
  - "Absolutely! Pay less for coverages and travel more throughout the year with our Multi-trip Annual plan. It's the best value we offer. Simply select the maximum number of days you plan to be away on any one trip and travel worry-free. No matter how often you travel, this Travel Medical Insurance goes wherever you go."
## friend driver
* chitchat/friend_driver
  - Everybody that's listed as a driver on another vehicle or policy can drive yourvehicle. Someone who already has insurance as a primary driver on another vehiclecan drive your car as well. If someone is not insured anywhere and drives yourvehicle, you should tell your insurance company so that person can be mentionedon your policy.
## grouping car insurance
* chitchat/grouping_car_insurance
  - With belairdirect, you get a discount when you insure two or more cars on thesame policy. It's our Multi-vehicle Discount.
## heating system leak coverage
* chitchat/heating_system_leak_coverage
  - No. You need to take out extra coverage for fuel oil leaks or the overflow ofa central heating system caused by a pipe or tank.Yes. If you have a fuel oilleak or an overflow of a central heating system caused by a pipe or tank, yourhome insurance covers the damage.Yes. If you have a fuel oil leak or an overflowof a central heating system caused by a pipe or tank, your home insurance coversthe damage.Yes. If you have a fuel oil leak or an overflow of a central heatingsystem caused by a pipe or tank, your home insurance covers the damage.
## high premium
* chitchat/high_premium
  - As you may already have noticed, student insurance premiums are usually some ofthe highest around. Generally speaking, this is because most students fall intoan age group that has a pretty high accident rate. Many of you are new to drivingand are still gaining experience behind the wheel, so there's a higher risk ofbeing involved in an accident. This higher risk leads to higher premiums.
## hit and run
* chitchat/hit_and_run
  - "To be covered for hit and run, you need to have Collision and upset Coverage. Keep in mind that for an event to be considered a hit and run, a police report must be filed within 24 hours. However, if you're not covered, you can request compensation from SAAQ. Please keep in mind that this doesn't replace coverage offered by insurance companies."
## home additional expenses
* chitchat/home_additional_expenses
  - "Yes, according to the terms, conditions and limitations of your policy. If you make a claim that's covered by your home insurance and you need to move out and get your meals somewhere other than your home, you're covered for any additional living expenses you face, compared to what you would normally have spent living in your home.For example, let's say you have major water damage to your home. You need to stay in a hotel for a month and eat your meals there while repairs are being made to your home. The total bill is $5,000.If the cost of your living expenses (e.g., meals, rent) for an equivalent period would normally have been $2,000, you would be compensated for the extra cost, in other words, $3,000.Your living expenses for one month during repairs - $5,000Your living expenses for one month under normal circumstances - $2,000Your claim payment for additional living expenses - $3,000"
## home condo insurance need
* chitchat/home_condo_insurance_need
  - "Unlike car owners- who must by law have car insurance to cover their civil liability, house and condo owners aren't legally required to have home insurance. However, it may be a requirement in certain situations. For example:Mortgaged homes                 If you want to take out a mortgage to pay for your home, your financial institution may require you to have home insurance. This helps protect the value of the home and the security of their loan in case of something unexpected, like a fire.Condo or co-op units                 If you're buying a condo or co-op unit, your co-owners association may require that you have insurance. You'll need to double-check with your co-owners association, as each is different.Whatever your living situation may be, we strongly recommend that you do get insurance because it not only protects your home and your personal belongings, it also gives you liability coverage in case you accidentally injure someone or damage someone's property anywhere in the world."
## home flood coverage
* chitchat/home_flood_coverage
  - No. A flood is considered an exceptional and catastrophic event and because ofthat, if your dwelling is located in an eligible territory, you have to take anextra coverage for this peril.Yes, only if your policy includes coverage for OverlandWater losses.Yes, only if your policy includes coverage for Overland Water losses.Yes, only if your policy includes coverage for Overland Water losses.
## home insurance limit
* chitchat/home_insurance_limit
  - Yes. There are special limits for certain items, such as jewellery, furs, bicycles,collections, boats, and money... Limits are different from policy to policy. Ifyou need to raise the amount of coverage on one of these items, you can add anendorsement to your policy or add a floater that protects something in particular.You can always adjust your insurance to make sure your coverage meets your uniqueneeds.
## home insurance requiremnts
* chitchat/home_insurance_requiremnts
  - Home insurance is always strongly recommended, whether you are a condo owner,tenant, owner or landlord. In home insurance, subrogation applies. This meansthat if you are the cause of the loss, the claimant's insurer will come back againstyou for the amount of damage related to the loss. It is therefore your civil liabilitythat will be involved. Your home insurance policy automatically covers you forthis. The amount varies according to certain criteria and needs.
## home insurance usual coverage
* chitchat/home_insurance_usual_coverage
  - "Your home insurance mainly covers your property and its contents. If you own a house, then your insurance will generally cover:The building itselfThe trees and shrubs located outdoors on the premisesDetached Structures on the premises, like a garage or shedYour own personal property, like furniture, electronics, computers, clothing, kitchenware, jewellery and artAdditional living expenses if you need to relocate temporarily after making a claimThe loss of any rental income after making a claim, if applicable.However, your needs are unique, so your coverage will be too. It depends whether your property is a principal or secondary residence, a cottage or condominium, an undivided co-ownership or an apartment. If you're insuring a condo, under certain conditions, these will be added on:Common portions which the co-owners have shared access or common portions for your exclusive useAny improvements and betterments you may make to your unit and to common portion for exclusive useImmovable elements of your unitYou can choose to insure your home for either of these:Most risks that come out of unexpected situations (all risks' coverage)Claims that come from risks included in your policy (designated risks overage)You can always customize your policy to get the right amount of insurance for your unique needs, like adding specific insurance for a special piece of jewellery or work of art.Most home insurance policies also give you Liability coverage, which protects you if you accidentally injure someone or damage someone's property, anywhere in the world."
## home water coverage
* chitchat/home_water_coverage
  - Yes, there is coverage for certain types of water damage losses. It is best tocheck your policy documents to confirm what types of water damage losses you arecovered for as it may differ based on your needs and what you have chosen.
## homeowner coverage
* chitchat/homeowner_coverage
  - "Your homeowners insurance policy covers your personal property and your liability. It also covers the same things for your spouse and your relatives who are living with you in the same household. It's a good idea to keep your policy up to date with the value of the belongings of everyone on the policy."
## house car insurance
* chitchat/house_car_insurance
  - If you insure your house and your car with belairdirect, you're eligible for ourCar + Home Discount.
## info confidentiality
* chitchat/info_confidentiality
  - As a valued customer of belairdirect, we respect your privacy and are committedto keeping personal information about you strictly between us. Our Privacy Policycan be found on our Website.
## injured action
* chitchat/injured_action
  - "If you are even slightly hurt in an accident, please follow the following steps. You may not realize the full extent of your injuries until later, so it's best for you to be as safe as possible. We'll be ready to support you.Call emergency services to the site of the accident and have them write out an accident report.As soon as possible, you should visit a doctor for an examination. In addition to making sure you're okay, your doctor should list all symptoms relating to your motor vehicle accident in your medical report. Have your doctor send you all reports to the Societe d'assurance automobile du Quebec (the "SAAQ", Quebec's public automobile insurance plan).Call us. We'll start your claim and support you through each step of the process until your claim is completed.Share medical information. On disclosure of injuries, you will be assigned an Accident Benefit Adjuster, who will handle all matters related to your injuries. They will review your benefits and provide you with the necessary forms required to be completed in order to start the payment of your benefits. If you are even slightly hurt in an accident, please follow the following steps. You may not realize the full extent of your injuries until later, so it's best for you to be as safe as possible. We'll be ready to support you.Call emergency services to the site of the accident and have them write out an accident report.As soon as possible, you should visit a doctor for an examination. In addition to making sure you're okay, your doctor should list all symptoms relating to your motor vehicle accident in your medical report. Call us. We'll start your claim and support you through each step of the process until your claim is completed.Share medical information. On disclosure of injuries, you will be assigned an Accident Benefit Adjuster, who will handle all matters related to your injuries. They will review your benefits and provide you with the necessary forms required to be completed in order to start the payment of your benefits."
## insurance home value
* chitchat/insurance_home_value
  - "At belairdirect, we consider everything it would take to rebuild your home entirely identical to the way it is right now. This includes the ground area covered by the building, the number of storeys, the existence of a garage, whether the basement is finished or not, and the year the house was built. When you choose your home insurance, you give us all the information we need to make these calculations. Please keep in mind that this isn't a reflection of the market value of your home, or how much you could sell your house for."
## intermediary definition
* chitchat/intermediary_definition
  - An intermediary is an individual between an insurance company and a consumer.This is having Civil Liability Coverage.
## landlord insurance coverage
* chitchat/landlord_insurance_coverage
  - The insurer covers the building, personal property within the dwelling such asfurniture, any detached private structures such as garages or gazebos, additionalliving expenses if ever you are unable to use your home and civil liability coverageif ever you damage somebody else's property.
## law requirement
* chitchat/law_requirement
  - If you want to drive a car, there's a minimum amount of insurance you need tohave. This minimum amount of insurance you must purchase through ICBC. Their mandatorybasic Autoplan insurance includes $200,000 Third Party Liability, Autoplan AccidentBenefits and Underinsured Motorist Protection.If you want to drive a car, there'sa minimum amount of insurance you need to have. This minimum amount of insuranceis outlined in our standard coverage chart. You'll also see options for increasingthe amount of coverage you have or buying more coverage.If you want to drive acar, there's a minimum amount of insurance you need to have. This minimum amountof insurance is outlined in our standard coverage chart. You'll also see optionsfor increasing the amount of coverage you have or buying more coverage.If youwant to drive a car in Quebec, there's a minimum civil liability insurance youneed to have. This minimum is $50,000 in Civil Liability, however, if you financeor lease your car, the financing company usually asks you to get complete coverage.This includes Liability, Collision and upset and All perils other than collisionor upset. Have a look at our easy-to-read coverage chart that outlines these sectionsand minimum coverage. You'll also see options for increasing the amount of coverageyou have.
## liab amout
* chitchat/liab_amout
  - The choice is really up to you. The $2,000,000 option is mainly used by peoplewho often travel outside the province or in the USA, where the insurance lawsare not always the same. With $ 2,000,000, you would be better protected in caseyou get sued after an accident.
## maintenance impact
* chitchat/maintenance_impact
  - "A fresh coat of paint, a hardtop, or wheel cover will definitely affect the value of your vehicle. If you're interested in exactly how, you can ask a claims adjuster. Regular maintenance checks - oil changes, sparkplug changes and mechanical work - will help keep your vehicle in good working order, but won't increase its value."
## maternity leave
* chitchat/maternity_leave
  - If you're driving a lot less now that you're not going to work every day, youshould have a chat with one of our certified insurance agents to see if you'reeligible for a reduction in your premium.
## medical conditions
* chitchat/medical_conditions
  - "Some medical conditions may affect the safe operation of a vehicle. You do not need to tell us which driver has any particular medical condition stated in the Declaration - or what that condition is. If there has been a recent change, simply answer 'Yes' and we will follow up with you directly to obtain further information. Please be assured that this information will be kept completely confidential.Unless there has been a recent change in medical conditions, which may affect the safe operation of your vehicle, you may simply respond 'No' to these questions."
## off-road-coverage
* chitchat/off-road-coverage
  - If your vehicle is new or fairly new, you can opt for coverage that ensures thatno depreciation will be applied to the value of your motorcycle, scooter, snowmobileor ATV in case of theft or a total loss.
## old cancellation info
* chitchat/old_cancellation_info
  - Yes. If you had an insurance policy cancelled for any reason during the past fiveyears, you need to let us know. This may affect your premium and our decisionto insure you.
## one week car insurance
* chitchat/one_week_car_insurance
  - If you do not have an insurance contract in force, unfortunately not. A contractis for a minimum period of one year. If you already have a contract in force,it is best to call your insurer and discuss your options with them.
## one-way two-way
* chitchat/one-way_two-way
  - Usually, one-way means you're only insured with basic coverage, meaning you onlyhave Civil Liability. This covers any bodily or material damage you cause to others.When you have two-way insurance, this usually means that your car is covered aswell as the damage you cause to others.
## online finance safety
* chitchat/online_finance_safety
  - You can transact your business online with belairdirect with confidence knowingbelairdirect uses the most up-to-date encryption technology available.All informationyou share with us is held in the strictest confidence, in compliance with privacystandards followed by all major Canadian financial institutions.
## other drivers
* chitchat/other_drivers
  - Everybody that's listed as a driver on your vehicle or policy. Someone who alreadyhas insurance as a primary driver on another vehicle can drive your car as well.If someone is not insured anywhere and drives your vehicle, you should tell yourinsurance company so that person can be mentioned (and covered) on your policy.
## outside province
* chitchat/outside_province
  - Yes, in Canada and the U.S. But if you're planning to use it for a trip that'sthree weeks or longer, you'll need to contact us so we can have a look at yoursituation and see if you have the best coverage for you
## parent or own insurance
* chitchat/parent_or_own_insurance
  - It's totally up to you. There are good reasons for or against each option. Forinstance, if you stay on your parents' policy, they can take advantage of multi-cardiscounts and possibly get better rates because of their life stage and stability.On the other hand, if your parents have an expensive car, their rates could goup because you're now driving it. If you get your own policy, you're in totalcontrol of your coverage options and payment terms. On the other hand, your ratesmay be higher because you can't take advantage of your parents' pricing.
## phone malfunction
* chitchat/phone_malfunction
  - It's the driver's responsibility to have a valid proof of insurance at all times.Be prepared for the unexpected by keeping a paper copy in your vehicle.
## policy definition
* chitchat/policy_definition
  - "A policy is a binding contract that outlines all the coverage you're getting from an insurance company."
## pool coverage
* chitchat/pool_coverage
  - "Your home insurance covers your inground swimming pool and spa for certain perils, but it is better to add an extra coverage. If f you have an above ground swimming pool and spa, you'll need to add an extra coverage.Ontario: Yes, your home insurance covers both these things. Your standard insurance policy protects you in case water seeps into your home because your pool or hot tub leaks."
## premium age 25
* chitchat/premium_age_25
  - 'We do take your age into consideration when we calculate your premium. Statistically,drivers who are 25 or older are less at risk. But there are many other factorsthat affect your premium, too: the type of car you drive, your claims record,how you use your vehicle, where you live, and many other factors. So althoughyour premium won''t automatically change once you turn 25, your age will be takeninto consideration when it''s time for your renewal.'
## premium calculation
* chitchat/premium_calculation
  - When we calculate your premium, we look at a lot of things, including:Your ageand driving experienceWhether your vehicle is for business, commuting or leisureTheyear, make, model and type of vehicleClaims you've made in the pastThe drivinghistory of any other people who will be behind the wheelEveryone's situation isdifferent, so everyone's premium will be too. It's important to get the rightamount of coverage for your unique situation so you're not overpaying or underinsured.
## premium definition
* chitchat/premium_definition
  - A premium is the amount you pay to an insurance company for your coverage.
## property damage definition
* chitchat/property_damage_definition
  - Property damage means damage to or destruction of property.
## reason inventory
* chitchat/reason_inventory
  - We strongly recommend that you keep an inventory of all your personal propertyto save yourself a ton of time, hassle and heartache if you ever need to makea claim. Keep your inventory up to date and tucked away in a safe place outsideof your home, like in a safety deposit box, along with your invoices, receipts,appraisals, photos, videos, make/model/serial numbers (or even user guides andwarranties). We suggest that you go from room to room and describe everythingin as much detail as possible.
## renovation communication
* chitchat/renovation_communication
  - "Yes. It's important to let us know before you begin renovations because we may need to revise the value of your home. You'll need to have enough insurance coverage to cover the value of your home, or the cost to rebuild it. You may also need extra coverage for the materials you use and for your own liability when performing renovations."
## rental car availability
* chitchat/rental_car_availability
  - "It depends. You'll qualify for a rental vehicle in these cases:If you've got a rental car option on your policy (whether or not you're at fault)If you've got belairdirect's AutoComfort coverage (whether or not you're at fault)If you only have standard coverage, and you're not at-fault"
## renter insurance requirements law
* chitchat/renter_insurance_requirements_law
  - "Although there's no law about having renters insurance, landlords can require a tenant to buy and maintain tenants insurance as a requirement of the lease. In addition, requiring tenants to purchase their own insurance policy is the only real way to protect their possessions."
## replacement actual cost difference
* chitchat/replacement_actual_cost_difference
  - "Replacement Cost is the cost of replacing something with a comparable new item or of repairing it (if it's the less expensive option) without an amount taken away for depreciation. If repairs are being done, only new materials of the same kind and quality will be used.Actual Cash Value is the actual value of something on the day the claim is made, not the value of it when it was new. This is calculated by taking the replacement cost then taking away an amount for depreciation, which is usually figured out by the condition of an item, its resale value, and its normal life expectancy.Here's an example: Let's say you bought a brand new TV for $1,500 in 2012 and it gets stolen one year later. Calculations show that the TV was worth $1,000 when it was stolen. With Replacement Cost on your policy, you would get a new TV that's identical or equivalent to the original. Without Replacement Cost, you would get $1,000 cash."
## replacement cost definition
* chitchat/replacement_cost_definition
  - Guaranteed Replacement Cost coverage means that if anything happens to your home(the actual residential building) the full cost to replace it (including constructioncosts) will be covered. Please speak with a licensed insurance agent to learnmore about this coverage.Enhanced Repair or Replacement Cost Without Deductionfor Depreciation coverage means that if anything happens to your home (the actualresidential building) the full cost to replace it (including construction costs)will be covered. Speak with a certified insurance agent to learn more.
## replacement seasonal vehicule
* chitchat/replacement_seasonal_vehicule
  - "We want you to travel with peace of mind. That's why you could purchase a coverage for a rental of a replacement vehicle in our standard insurance coverage. Coverage includes damage to your seasonal vehicle due to an accident, up to $2,000 for a rental vehicle, as well as additional expenses, such as meals, lodging, transportation of your personal effects, and if required, bringing your vehicle home, up to a total of $1,000. * Does not apply to mopeds 50cc or less"
## saving car premium
* chitchat/saving_car_premium
  - We've got good news. If you're under the age of 25 and are a full-time studentat a recognized College or University, you're eligible to save up to 10% on yourcar insurance with belairdirect. You can also consider adding The Accident Forgivenessto your coverage to protect against your car insurance premium going up as a resultof your first at-fault accident. You can also consider raising your deductible.And it goes without saying that being a good driver is always the best way tokeep your premium from going up.
## seasonal car saving
* chitchat/seasonal_car_saving
  - Take control of your off-road vehicle and take advantage of discounts for whichyou may be eligible.Get a discount if:Your vehicle has an anti-theft device, intensiveengraving or tracking system (recognized by belairdirect).You insure more thanone off-road vehicle with belairdirect.You're a member of a club or associationrecognized by belairdirect.Plus, you can save on your belairdirect home insuranceby combining it with your off-road vehicle insurance.
## seasonal vehicule insurance
* chitchat/seasonal_vehicule_insurance
  - 'Whether you use your motorcycle, scooter or moped as your primary method of transportationduring the summer months or just for weekend trips, you need insurance that protectsyou on the road. belairdirect offers insurance products for the following specialvehicles:MotorcyclesSnowmobilesATVsFifth wheelsTravel trailersMotorhomesTractorsTaxisDoyou need coverage for vehicles other than your car(s)? belairdirect can help.We also offer insurance products for the following special vehicles:SnowmobilesATVsFifthwheelsTravel trailersWhether you use your motorcycle as your primary method oftransportation during the summer months or just for weekend trips, you need insurancethat protects you on the road. belairdirect offers insurance products for thefollowing special vehicles:MotorcyclesFifth wheelsTravel trailersMotorhomesAlberta:Whether you use your motorcycle, scooter or moped as your primary method of transportationduring the summer months or just for weekend trips, you need insurance that protectsyou on the road. belairdirect offers insurance products for the following specialvehicles: MotorcyclesSnowmobilesATVsFifth wheelsTravel trailersMotorhomes'
## second car premium
* chitchat/second_car_premium
  - Your premium will increase when you insure another car. But by insuring more thanone vehicle with belairdirect, you can take advantage of our Multi-vehicle Discount.
## session encryption
* chitchat/session_encryption
  - 'You know that your data has been encrypted on a given Web page by looking forthe following icons in the lower portion of your browser:Internet ExplorerMozillaFirefoxSafariNOTE: Microsoft Internet Explorer and Firefox display the icon onthe lower right corner of the browser. In addition, Safari displays the icon inthe upper left navigation toolbar.With Microsoft Internet Explorer and Firefoxyou can click on the icon to determine what level of encryption is being usedfor a particular Web page.We ensure all online sessions with belairdirect areencrypted.Please note the installation, downloading, or use of certain softwaremay result in the ability of third parties being able to analyse or collect yourinformation by redirecting secure transmissions through their servers. While wewill endeavour to protect all information as much as possible by notifying youof such redirection and by blocking access when such software is detected by belairdirect,we cannot guarantee the security and privacy of any information should you decideto use such software, and belairdirect shall not be liable due to its inabilityor failure to provide notice of redirection of your information, or for its inabilityor failure to block access when such software is detected. You acknowledge thatbelairdirect will not be liable for any damages arising from the use of such softwareirrespective of whether we are aware of such risks.'
## settlement definition
* chitchat/settlement_definition
  - A settlement is what an insurance company pays out for your covered claim.
## short term rental need
* chitchat/short_term_rental_need
  - Yes. Short-term rental (through services such as Airbnb, HomeAway, Kijijiji etc.)leads to an increased risk of claims. We consider that the risk is increased fromthe first day of a rental. That is why a rate is applied at all times in thesesituations.
## single trip insurance
* chitchat/single_trip_insurance
  - "Of course you can! If you're planning on travelling only once this year, Single Trip coverage is the most economical way to travel protected. Rates will vary depending on if you're travelling within Canada, to the United States or somewhere else in the world."
## stolent action
* chitchat/stolent_action
  - "It's a terrible feeling to discover that your vehicle has been stolen. We can help you to recover your belongings. Remember that in addition to your car, your insurance may cover some of the items inside it (as long as they were used only with your car).Check if your car was towed. Look at nearby signs to see if there are parking restrictions where you parked. You may want to call a towing company to double check that the vehicle was not towed. If you suspect your car has been stolen, report the incident to the police.If you have a tracking system or tracking device, call the company to begin a search for your vehicle.Call us. Our claims representatives can start your claim and help you decide what to do next.To process your claim properly, you'll need to have these details ready: registration certificate, driver's licence, purchase/rental contract, make/model/serial number of your car, invoices (for repairs, maintenance and added equipment) and keys. You should also make a list of the items that were in the vehicle when it was stolen.Talk to a claims adjuster. Within 24-48 hours of you opening your claim, a claims adjuster will call you to complete your statement.When your car was stolen, it may have contained items that weren't covered by your car insurance policy. However, they may be covered by your home insurance or other insurance policy, depending on your coverage"
## tenant insurance bulding damage
* chitchat/tenant_insurance_bulding_damage
  - "Tenants are held financially responsible for the harm they cause to any part of the building in which they live or to others who live or visit there. Your liability coverage will protect you if you cause accidental damage to the building - whether to your unit or to the whole building."
## tenant insurance need
* chitchat/tenant_insurance_need
  - "Although there's no law about having tenants insurance, there sure are a lot of compelling reasons. So many unexpected things can happen, like a fire or a break-in. If either of these happened, it could cost you thousands to replace your belongings, things like furniture, electronics, appliances, bedding, and clothes.Your tenants insurance would cover these things, and the cost of temporary accommodation if you need to move out when repairs are being done. What's more, if you accidentally injure someone or damage another person's property anywhere in the world, your liability coverage will protect you, up to the amount of your insurance coverage."
## third party offer
* chitchat/third_party_offer
  - We strongly recommend that you let your insurance companies settle your claims.We've seen private settlements end badly in the past, because problems can definitelycome up. You could be left to foot the bill and you'd have no way of then claimingit through your insurance company. Your best bet is to let us deal with everythingin a fair and equitable way.
## total loss
* chitchat/total_loss
  - This means that it would cost more to repair it than what the car is actuallyworth. You might be offered a settlement based on the car's cash value, dependingon your coverage.
## total loss action
* chitchat/total_loss_action
  - "A vehicle is a total loss if it would cost more to repair it than what the car is actually worth.Talk to the claims adjuster who is handling your file. A claims adjuster will contact you and inform you of the next steps.Find all applicable documents. A copy of your registration, sale or lease contract and repair invoices, are just a few items that will be requested.Determine the value of your car. Upon receipt of the requested documents, your adjuster will be in a position to provide you with your vehicle's value.Your vehicle will need to be emptied of all personal belongings.Your consent is needed to move your vehicle to one of our recycling companies to minimize costs until the settlement of the file. Only then will the ownership of the car be transferred to belairdirect's name.Your vehicle's value is established by your insurer depending on the insurance coverage in effect when the loss occured."
## travel insurance car rental
* chitchat/travel_insurance_car_rental
  - If you'd like, you can choose to add a Rental Car Protection Coverage to yourtravel insurance.Rental Car Protection provides coverage worldwide against physicaldamage or loss of a car when you rent or lease during your trip. Up to $50,000worldwide against physical damage and loss of a car you rent or lease automobilecar on your trip.
## travel insurance claim
* chitchat/travel_insurance_claim
  - "Wherever you travel in the world, assistance is only a toll-free phone call away. In the event of a medical emergency, friendly Claims Representatives are available 24/7.\*In Canada & USA, call 1 800 663.0399In Mexico, call 001 800 514.9976 or 01 800 681.8070Worldwide, call collect 1 604 278.4108To open your claim online, please visit tugo.com/claims. It's convenient and easy to use."
## travel insurance coverage
* chitchat/travel_insurance_coverage
  - "belairdirect features a wide variety of insurance products -so you can choose the options that are right for you!Travel Medical Insurance - Multi-trip AnnualTravel Medical Insurance - Single TripTravel Medical Insurance - Visitors to CanadaAdditional plans & optionsOther available optionsProtect yourself today!"
## trip belongings coverage
* chitchat/trip_belongings_coverage
  - "Yes. Any personal property you take with you while you're temporarily away from home are covered by your home insurance policy. Keep in mind that there are some exceptions, so please have a look at your policy or talk with one of our certified insurance agents in case you're not sure."
## trip cancel coverage
* chitchat/trip_cancel_coverage
  - "To be covered, you need to have Trip Cancellation /Trip Interruption Coverage. Keep in mind that for an event to be considered a hit and run, a police report must be filed within 24 hours. However, if you're not covered, you can request compensation from SAAQ. Please keep in mind that this doesn't replace coverage offered by insurance companies. Trip Cancellation/Trip Interruption coverage reimburses certain travel costs if your travel plans are cancelled before your departure or if they are disrupted after the start of your trip because of a covered risk, such as your unexpected sickness, injury and death or that of your travelling companion, your immediate family or your travelling companion's immediate family, natural disaster, travel advisory, subpoena, hijacking, involuntary job loss, missed connection and non-issuance of a travel visa."
## underwriter definition
* chitchat/underwriter_definition
  - An underwriter is a person who looks at all your situation, evaluates your risksand calculates your premium.
## vacant home insurance
* chitchat/vacant_home_insurance
  - "It's never ideal to leave a house vacant since the nature of the risk changes considerably. This has consequences for your insurance coverage because it no longer fits the situation. If it is unavoidable call us, preferably in advance, so that we can assess the situation with you."
## visitor medical insurance
* chitchat/visitor_medical_insurance
  - Our Travel Medical Insurance covers people visiting Canada, new immigrants orCanadians returning from abroad who are awaiting provincial medical coverage.This coverage is available for Single Trip only.This insurance covers visitorsto Canada requiring an emergency hospital stay or emergency medical treatment.It also protects recent immigrants and Canadians returning to Canada after livingabroad who are awaiting provincial health plan coverage.Some of the benefits includemedical transport, emergency dental, prescription drugs and additional board andlodging expense for your travelling companion while you are confined to a hospital.Youcan choose coverage options with limits from $10,000 to $300,000 CAD.
## waiver definition
* chitchat/waiver_definition
  - A waiver is something you sign to surrender one of your rights.
## water damage action
* chitchat/water_damage_action
  - Call our 24/7 claims department at 1 877 228.2656 1 877 270.9124. A claims representativecan guide you through the next steps and give you helpful advice. After confirmingyour coverage, an adjuster will help arrange for a contractor to come in to helpget rid of the water, clean up the damage, and find the cause.
## water damage experience
* chitchat/water_damage_experience
  - "Chances are, you will face water damage in your home at some point. But just because it's a common occurrence doesn't make it any less inconvenient or costly. That's why we'll be ready to help you get your house dried out and your possessions restored as soon as possible.Check to see if your house is structurally intact. Make sure the power and heat are turned off in the flooded area. We don't want you or anyone in your family to be hurt.Call us. Our claims representatives can start your claim and help you decide what to do next.If the water damage has made it impossible to live in your home, one of our claims representatives can help you relocate until your home is back to normal.Write a list of damaged or lost items and include their purchase date and value, with receipts.Take photos of any water damage in your home and any damaged personal property belongings. Wear gloves and boots to clean and disinfect if needed. Prevent mould by removing wet contents as soon as possible. Open windows and use fans to help dry the area.Call City Hall and let them know about the situation, in case your water damage was related to the city sewers.After water damage, some items in your home may need to be thrown out. Don't forget to take a picture first, so you can include those items in your claim."
## who total loss
* chitchat/who_total_loss
  - It depends. If a claim settlement gets the green light, the payment can go eitherto you (if you own the vehicle), or to the lessor company (if your vehicle isleased), or jointly to you and to the finance company (if your vehicle is financed).
