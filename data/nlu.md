## intent:chitchat/US_coverage
- We're planning a long roadtrip to the U.S. Do we need to change our car insurance coverage?

## intent:chitchat/aging_car_premium
- As my car gets older, why doesn't my car insurance premium go down?

## intent:chitchat/at_fault_decision
- How does an insurance company decide who's at fault?

## intent:chitchat/at_fault_definition
- What is an at-fault accident?

## intent:chitchat/attendant_definition
- What is an attendant?

## intent:chitchat/atv_outside_province
- We're planning an ATV trip outside the province. Do we need to change our off-road insurance coverage?

## intent:chitchat/automerit_additional_drivers
- Can additional drivers on my policy join automerit?

## intent:chitchat/automerit_battery_usage
- How much of my smartphone's battery will be used while using automerit?

## intent:chitchat/automerit_behaviour_display
- What driving behaviour is displayed in the app?

## intent:chitchat/automerit_business_use
- What happens with automerit if I drive my car for business?

## intent:chitchat/automerit_calculation
- How is my discount calculatedHow do you calculate my discount or surcharge?

## intent:chitchat/automerit_call_tracking
- Do you track my phone calls?

## intent:chitchat/automerit_data_plan
- Do I need to have a data plan to use automerit?

## intent:chitchat/automerit_data_protection
- How is my driving data protected?

## intent:chitchat/automerit_data_sharing
- Will belairdirect ever share my information?

## intent:chitchat/automerit_data_usage
- How much of my data plan will be used for automerit?

## intent:chitchat/automerit_definition
- What's automerit and how does it work?
- What is automerit?

## intent:chitchat/automerit_delete_app
- What happens if I delete the belairdirect app?

## intent:chitchat/automerit_discount_application
- How is my discount applied to my car insurance?

## intent:chitchat/automerit_discount_mismatch
- Why does my app discount not match my policy documents?

## intent:chitchat/automerit_discount_storage
- What happens to my discount if my car is in storage? What happens to my discount or surcharge if my car is in storage?

## intent:chitchat/automerit_discount_update_time
- When is my discount updated in the app?

## intent:chitchat/automerit_download
- How do I download automerit?

## intent:chitchat/automerit_driver_detection
- How do you know if I'm the driver or just a passenger while using automerit?

## intent:chitchat/automerit_driving_data_police
- Can the police access my driving data if I have a car accident?

## intent:chitchat/automerit_driving_stats
- What are my Driving Stats?

## intent:chitchat/automerit_enrolment
- How do I join automerit?

## intent:chitchat/automerit_enrolment_discount
- What's an enrolment discount?What is an enrolment discount?

## intent:chitchat/automerit_error_management
- What should I do if there's a mistake in my driving data?

## intent:chitchat/automerit_info_other_drivers
- Can other drivers on my policy see my Safety Score and trips?

## intent:chitchat/automerit_mileage_need
- How many kilometres do I need to drive?

## intent:chitchat/automerit_missing_trip
- Why don't I see my latest trip?

## intent:chitchat/automerit_multiple_cars
- Can my automerit discount apply to all my cars? Can my discount or surcharge apply to all my cars?

## intent:chitchat/automerit_need_call
- Why do you need permission to make and manage phone calls on my Android device for automerit?

## intent:chitchat/automerit_need_location
- Why do you need permission to use my location for automerit?

## intent:chitchat/automerit_need_sensor
- Why do you need permission to access my iPhone's motion sensors for automerit?

## intent:chitchat/automerit_new_number
- What do I do if I get a new phone number?

## intent:chitchat/automerit_new_phone
- What happens if I get a new or different type of smartphone?

## intent:chitchat/automerit_no_activation
- What happens if I don't activate automerit?

## intent:chitchat/automerit_no_driving
- What happens if I don't drive for a while?

## intent:chitchat/automerit_not_driver_history
- I wasn't the driver! How do I change my Trip Summary?

## intent:chitchat/automerit_not_in_app
- "I\u2019ve followed all the steps. Why doesn\u2019t automerit appear in my app?"

## intent:chitchat/automerit_passenger_using_phone
- Someone else was using my phone while I was driving. What now?

## intent:chitchat/automerit_payment_adjustment
- How will my car insurance payments be adjusted?

## intent:chitchat/automerit_premium_update
- When will the cost of my car insurance change?

## intent:chitchat/automerit_safety_score_definition
- What's my Safety Score?

## intent:chitchat/automerit_safety_score_reading
- How do I read my Safety Score?

## intent:chitchat/automerit_see_discount
- 'How do I know if I''m getting a discount? How do I know what my discount or surcharge will be? '

## intent:chitchat/automerit_smartphone
- What type of smartphone do I need to use automerit?

## intent:chitchat/automerit_start_mismatch
- Why does my trip's starting point not match my actual location?

## intent:chitchat/automerit_surcharge
- Can the cost of my car insurance increase because of automerit?

## intent:chitchat/automerit_trip_access
- Who has access to my trips?

## intent:chitchat/automerit_uber
- Can I join automerit if I'm an Uber, Lyft or commercial driver?

## intent:chitchat/automerit_withdrawral
- Can I withdraw from automerit at any time? Can I withdraw from the program at any time?

## intent:chitchat/basic_property_insurance
- What are the two basic forms of property insurance?

## intent:chitchat/belongings_in_car
- Are my personal belongings inside my car covered in case of a theft?

## intent:chitchat/belongings_replacement_value
- I paid $1,200 for a new laptop computer in 2009. If it were stolen today, would I get $1,200 back from insurance?

## intent:chitchat/burgler_action
- What to do if my home has been burglarized?

## intent:chitchat/car_damage_action
- What do I do if there is damage to my car?

## intent:chitchat/car_insurance_discount
- How can I get a discount on my car insurance?

## intent:chitchat/car_insurance_obligation
- Do I need to have car insurance?

## intent:chitchat/car_market_value
- How do you figure out the cash value of my car?

## intent:chitchat/car_repairs
- If I've been in an accident, where should I go for car repairs?

## intent:chitchat/car_storage
- If my car is in storage, should I still need insurance?

## intent:chitchat/car_type_impact
- Does the kind of car I drive affect my premium?

## intent:chitchat/catastrophic_injury_definition
- What is a catastrophic injury?

## intent:chitchat/child_learning
- My son/daughter has a learner's permit; do I have to add him/her on my policy?

## intent:chitchat/city_to_suburb
- We're moving from the city to the suburbs. Will this affect my premium?

## intent:chitchat/civil_liability_definition
- What does civil liability mean?

## intent:chitchat/claim_documents
- What documents will I need to give you if I make a claim?

## intent:chitchat/claim_impact_premium
- If I make a claim, will it affect my insurance premium?

## intent:chitchat/commercial_car_personal_use
- Does commercial auto insurance cover personal use?

## intent:chitchat/daughter_car_insurance
- My daughter wants to get her licence. How can she save on her car insurance down the road?

## intent:chitchat/deductible_explanation
- What is a deductible?

## intent:chitchat/dependant_definition
- What is a dependant?

## intent:chitchat/depreciation_definition
- What is depreciation?

## intent:chitchat/difference_partial_total_loss
- What is the difference between a partial loss and total loss?

## intent:chitchat/digital_insurance_access
- How do I access my digital proof of insurance?

## intent:chitchat/digital_insurance_commercial
- Can I use my digital proof of insurance if I drive a commercial car, motorcycle or an ATV?

## intent:chitchat/digital_insurance_info
- What information is on the digital proof of insurance?

## intent:chitchat/digital_insurance_offline
- How can I make sure that my digital proof of insurance is up to date even when I'm offline?

## intent:chitchat/digital_insurance_outside_province
- Can I use my digital proof of insurance outside of my province?

## intent:chitchat/digital_insurance_personal_info_protection
- How do I protect my personal information when showing my digital proof of insurance to the police or other drivers?

## intent:chitchat/digital_insurance_phone_damage
- What happens if my phone is damaged when I share my digital proof of insurance?

## intent:chitchat/digital_insurance_print
- Can I print a copy of my proof of insurance?

## intent:chitchat/digital_insurance_proof
- 'Can I use my digital proof of insurance instead of the paper version? '

## intent:chitchat/digital_insurance_refusal
- What should I do if a police officer does not accept my digital proof of insurance?

## intent:chitchat/digital_insurance_share
- If someone else drives my car, how do I share my digital proof of insurance with them?

## intent:chitchat/domestic_international_encryption
- What's the difference between domestic-grade encryption and international-grade encryption?

## intent:chitchat/encryption_definition
- How does encryption work?

## intent:chitchat/encryption_need
- What type of encryption do I need?

## intent:chitchat/endorsement_definition
- What is endorsement?

## intent:chitchat/equipment_coverage
- Are my equipment and accessories covered?

## intent:chitchat/fire_damage_action
- What to do when I experience a fire in my home?

## intent:chitchat/fire_insurance_coverage
- What is covered under fire insurance policy?

## intent:chitchat/fraud_prevention
- How do you prevent fraud?

## intent:chitchat/frequent_flyers
- Do you have annual plans for frequent travelers?

## intent:chitchat/friend_driver
- Can my friends/roommates drive my car?

## intent:chitchat/grouping_car_insurance
- Is it cheaper to insure my car and my husband's car together or separately?

## intent:chitchat/heating_system_leak_coverage
- Does my home insurance cover damage caused by a fuel oil leak from my central heating system?

## intent:chitchat/high_premium
- Why is my car insurance premium so high?

## intent:chitchat/hit_and_run
- When am I covered for hit and run?

## intent:chitchat/home_additional_expenses
- Does my home insurance cover any additional expenses I have if I need to move out temporarily while repairs are being done after I make a claim?

## intent:chitchat/home_condo_insurance_need
- If I own a house or condominium, do I have to get home insurance?

## intent:chitchat/home_flood_coverage
- Does my home insurance cover damage caused by a flood?

## intent:chitchat/home_insurance_limit
- Does my belairdirect home insurance have any limits for coverage?

## intent:chitchat/home_insurance_requiremnts
- Who needs home insurance?

## intent:chitchat/home_insurance_usual_coverage
- What does home insurance usually cover?

## intent:chitchat/home_water_coverage
- Does my home insurance cover any kind of water damage?

## intent:chitchat/homeowner_coverage
- I have a homeowners insurance policy under my name. Who does it cover?

## intent:chitchat/house_car_insurance
- We just bought a house. Will this help us save on car insurance?

## intent:chitchat/info_confidentiality
- How can I be sure that this information will be kept confidential?

## intent:chitchat/injured_action
- What do I do if I am injured in an accident?

## intent:chitchat/insurance_home_value
- When it comes to insurance, how is the value of my home calculated?

## intent:chitchat/intermediary_definition
- What is intermediary?

## intent:chitchat/landlord_insurance_coverage
- What is covered by landlord insurance?

## intent:chitchat/law_requirement
- How much insurance do I need to have by law?

## intent:chitchat/liab_amout
- What's the difference between $1,000,000 and $2,000,000 liability?

## intent:chitchat/maintenance_impact
- I spend a lot of money maintaining my vehicle. Will this help boost its value?

## intent:chitchat/maternity_leave
- We just had a baby and I'm on maternity leave. Will I need to change anything on my policy?

## intent:chitchat/medical_conditions
- Why are medical conditions of licensed drivers in my household included in Additional Information?

## intent:chitchat/off-road-coverage
- Is my off-road vehicle covered in case of theft or total loss?

## intent:chitchat/old_cancellation_info
- Do I need to tell you about a previous policy that was cancelled by another insurance company? Will this affect my premium?

## intent:chitchat/one-way_two-way
- What's the difference between one-way and two-way insurance?

## intent:chitchat/one_week_car_insurance
- Can I insure a car for a week?

## intent:chitchat/online_finance_safety
- Are Online Financial Services Safe?

## intent:chitchat/other_drivers
- Who else can drive my car?

## intent:chitchat/outside_province
- Can I use my car outside of the province?

## intent:chitchat/parent_or_own_insurance
- Is it better to get my own insurance when I go to College? Or should I stay on my parents' policy?

## intent:chitchat/phone_malfunction
- What happens if my phone malfunctions or its battery dies?

## intent:chitchat/policy_definition
- What is a policy?

## intent:chitchat/pool_coverage
- Are my swimming pool and spa covered by my home insurance?

## intent:chitchat/premium_age_25
- Will my car insurance go down when I turn 25?

## intent:chitchat/premium_calculation
- How do you calculate my car insurance premium?

## intent:chitchat/premium_definition
- What is a premium?

## intent:chitchat/property_damage_definition
- What is considered property damage?

## intent:chitchat/reason_inventory
- Why should I create a personal inventory of everything I own?

## intent:chitchat/renovation_communication
- "If I\u2019m doing renovations on my house, do I need to tell my insurance company?"

## intent:chitchat/rental_car_availability
- If I've been in an accident, do I get a rental vehicle?

## intent:chitchat/renter_insurance_requirements_law
- Is it legal to require renters insurance?

## intent:chitchat/replacement_actual_cost_difference
- "What\u2019s the difference between Replacement Cost and Actual Cash Value?"

## intent:chitchat/replacement_cost_definition
- What is Guaranteed Replacement Cost coverage? Does belairdirect offer it?What is Enhanced Repair or Replacement Cost Without Deduction for Depreciation? Does belairdirect offer it?

## intent:chitchat/replacement_seasonal_vehicule
- Is there a rental of a replacement vehicle included in my standard seasonal vehicle insurance coverage?

## intent:chitchat/saving_car_premium
- How can I save on my car insurance premium?

## intent:chitchat/seasonal_car_saving
- How can I save on my seasonal car insurance premium?

## intent:chitchat/seasonal_vehicule_insurance
- Do I need insurance for a seasonal vehicle?

## intent:chitchat/second_car_premium
- We're thinking of getting a second car. How much will our premium go up?

## intent:chitchat/session_encryption
- How do I know if my online session is encrypted?

## intent:chitchat/settlement_definition
- What is a settlement?

## intent:chitchat/short_term_rental_need
- Do I need extra insurance for short-term rental?

## intent:chitchat/single_trip_insurance
- Can I get insured for a single trip?

## intent:chitchat/stolent_action
- What do I do if my car has been stolen?

## intent:chitchat/tenant_insurance_bulding_damage
- Does tenant insurance cover damage to the occupied home and/or the building?

## intent:chitchat/tenant_insurance_need
- "If I\u2019m a tenant why would I get home insurance?"

## intent:chitchat/third_party_offer
- I've been in an accident and the other person offered to pay for the damages. Should I let them?

## intent:chitchat/total_loss
- My car is a total loss. What does that mean for me?

## intent:chitchat/total_loss_action
- What do I do if my car is a total loss?

## intent:chitchat/travel_insurance_car_rental
- Does my travel insurance cover car rental insurance?

## intent:chitchat/travel_insurance_claim
- How do I make a travel insurance claim?

## intent:chitchat/travel_insurance_coverage
- What does travel insurance cover?

## intent:chitchat/trip_belongings_coverage
- If I go on a trip, are the things I take with me covered by my insurance?

## intent:chitchat/trip_cancel_coverage
- When am I covered for Trip Cancellation /Trip Interruption?

## intent:chitchat/underwriter_definition
- What is an underwriter?

## intent:chitchat/vacant_home_insurance
- Can I insure a vacant home?

## intent:chitchat/visitor_medical_insurance
- I plan on visiting Canada. Do you offer travel medical insurance for visitors?

## intent:chitchat/waiver_definition
- What is a waiver?

## intent:chitchat/water_damage_action
- What should I do if my home gets damaged by water?

## intent:chitchat/water_damage_experience
- What to do when I experience water damage?

## intent:chitchat/who_total_loss
- If my vehicle is a total loss, who does my insurance company pay?

## intent:greet
- Hi
- Hi!
- Hello

## intent:out_of_context
- What is love?
